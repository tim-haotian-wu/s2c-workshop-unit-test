import React from "react";
import Counter from ".";
import { shallow, mount } from "enzyme";
import { Typography } from "antd";
const { Title } = Typography;

describe("Counter", () => {
  test("the UI should always match previous stored snapshot unless there are expected changes will affect the snapshot", () => {
    const tree = shallow(<Counter initialValue={0} />);
    expect(tree).toMatchSnapshot();
  });

  test("user can increment the count value by 1 after click increment button", () => {
    const wrapper = mount(<Counter initialValue={0} />);
    const incrementButton = wrapper.find("button").at(0);
    const count = wrapper.find(Title).at(0);
    incrementButton.simulate("click");
    expect(count.getDOMNode()).toHaveTextContent("1");
  });
});
