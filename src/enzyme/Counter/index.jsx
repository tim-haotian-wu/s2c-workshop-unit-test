import React, { useState } from "react";
import { Button, Typography, Row, Col, Space } from "antd";
import "antd/dist/antd.css";
import "./Counter.css";
const { Text, Title } = Typography;

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: props.initialValue
    };
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  shouldComponentUpdate() {
    console.log("shouldComponentUpdate");
    return true;
  }

  componentWillReceiveProps() {
    console.log("componentWillReceiveProps");
  }

  componentDidUpdate() {
    console.log("componentDidUpdate");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  render() {
    const { initialValue } = this.props;
    const { count } = this.state;
    return (
      <Row className="counter-container" justify="center" align="middle">
        <Col>
          <Row justify="center" align="middle">
            <Title data-testid="count">{count}</Title>
          </Row>
          <Row justify="center" align="middle">
            <Col>
              <Space>
                <Button
                  type="primary"
                  onClick={() =>
                    this.setState({
                      count: count + 1
                    })
                  }
                >
                  increment
                </Button>
                <Button
                  type="primary"
                  onClick={() =>
                    this.setState({
                      count: count - 1
                    })
                  }
                >
                  decrement
                </Button>
                <Button
                  type="primary"
                  onClick={() =>
                    this.setState({
                      count: initialValue
                    })
                  }
                >
                  reset
                </Button>
              </Space>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Counter;
