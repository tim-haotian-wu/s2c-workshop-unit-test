import React from "react";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Counter from ".";
// import mocked from "./file2mock";
// jest.mock("./file2mock");

describe("Counter", () => {
  test("the UI should always match previous stored snapshot unless there are expected changes will affect the snapshot", () => {
    const tree = render(<Counter initialValue={0} />);
    expect(tree).toMatchSnapshot();
  });

  test("user can increment the count value by 1 after click increment button", () => {
    const { getAllByRole, getByText, getByTestId } = render(
      <Counter initialValue={0} />
    );
    const incrementButton = getByText("increment");
    const count = getByTestId("count");
    userEvent.click(incrementButton);
    // console.log("getAllByRole", getAllByRole("button")[0]);
    expect(count).toHaveTextContent(1);
  });
});
