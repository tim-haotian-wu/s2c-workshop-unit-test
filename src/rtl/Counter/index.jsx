import React, { useState } from "react";
import { Button, Typography, Row, Col, Space } from "antd";
import "antd/dist/antd.css";
import "./Counter.css";
const { Text, Title } = Typography;

function Counter({ initialValue }) {
  const [count, setCount] = useState(initialValue);
  return (
    <Row className="counter-container" justify="center" align="middle">
      <Col>
        <Row justify="center" align="middle">
          <Title data-testid="count">{count}</Title>
        </Row>
        <Row justify="center" align="middle">
          <Col>
            <Space>
              <Button type="primary" onClick={() => setCount(count + 1)}>
                increment
              </Button>
              <Button type="primary" onClick={() => setCount(count - 1)}>
                decrement
              </Button>
              <Button type="primary" onClick={() => setCount(initialValue)}>
                reset
              </Button>
            </Space>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default Counter;
