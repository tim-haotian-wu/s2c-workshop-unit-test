import React from "react";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import TeamList from ".";
// https://testing-library.com/docs/intro

describe("TeamList page", () => {
  test("User can see a list of names of team members", () => {
    // list of text/divs ?
    // default values?
  });

  test("User can add a new team member by input their names and click submit button", () => {
    // a submit button?
    // a text input?
  });

  test("User can select checkbox beside each members name and click delete button to remove them from the list", () => {
    // checkbox beside each person's name?
    // a delete button?
  });

  // bonus:
  // use setTimeout in the component to delay 3 seconds for update
  test("User can click reset button and wait for 3 seconds to reset team members to default", () => {});

  // make the coverage to 100%;
  // Thanks all!
});
